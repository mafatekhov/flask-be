# flask-be


This web-server was created by using Flask Python framework. It is a simple REST server, that stores my articles in the PostgreSQL database. The frontend part of the site is enabled with the jinja2 template engine.
The site that contains short training articles written by me, was created to examine in detail the some main topics and try to describe them in simple language.
Answers to questions and examples are also provided.


**Install prerequisites**
1. Create virtualenv, run the following command:
```bash
virtualenv programmimg_blog
```
2. Activate virtualenv:
```bash
source programmimg_blog/bin/activate
```
3. Install prerequisites by running:
```bash
pip install -r reqiorments.txt
```

 ***Run the server***
```
 export FLASK_APP=server.py 
```
```
flask run
```

## Endpoints
##### **- @app.route("/", methods=["GET"])**

Endpoint used to generate an up-to-date list of articles that are stored in the database and displays them on the website page.

##### **- @app.route("/create_article", methods=['POST'])**

Endpoint used to create a new article in the database.

##### **- @app.route('/create_article', methods=["GET"])**

Endpoint used to show the created article.

##### **- @app.route('/contents/<int:id>', methods=["GET"])**

Endpoint used to display the id, topic, title, and article and allows you to remove it from the database.

##### **- @app.route('/del_article/<int:article_id>', methods=["POST"])**

Endpoint used to delete the article from the database.