from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask_heroku import Heroku

from flask import (Flask, render_template, request, redirect, url_for)

app = Flask(__name__)
app.config.from_object(__name__)
CORS(app)
# app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:@localhost/test_flask'
heroku = Heroku(app)
db = SQLAlchemy(app)


class Contents(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    topic = db.Column(db.String(200))
    subtopic = db.Column(db.String(200))
    content = db.Column(db.Text())

    def __init__(self, topic, subtopic, content):
        self.topic = topic
        self.subtopic = subtopic
        self.content = content


@app.route("/", methods=["GET"])
def to_parse():
    new_dict = {}
    list_of_articles = Contents.query.all()
    for article in list_of_articles:
        my_id = article.id
        my_topic = article.topic
        my_subtopic = article.subtopic
        if my_topic in new_dict.keys():
            new_dict[my_topic][my_subtopic] = my_id
        else:
            new_dict[my_topic] = {my_subtopic: my_id}
    return render_template('index.html', list_ar=new_dict)


@app.route("/about", methods=["GET"])
def about():
    return render_template('about.html')


@app.route("/create_article", methods=['POST'])
def create_article():
    new_article = Contents(request.form['topic'], request.form['subtopic'], request.form['content'])
    db.session.add(new_article)
    db.session.commit()
    return redirect(url_for('to_parse'))


@app.route('/create_article', methods=["GET"])
def new_article():
    return render_template('new_article.html')


@app.route('/contents/<int:id>', methods=["GET"])
def show_contents(id):
    content = Contents.query.filter_by(id=id).first()
    return render_template('show_contents.html', content=content, article=content.content)


@app.route('/del_article/<int:article_id>', methods=["POST"])
def del_article(article_id):
    article = Contents.query.filter_by(id=article_id).first()
    db.session.delete(article)
    db.session.commit()
    return render_template('del_article.html', article=article)
