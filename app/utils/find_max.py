def find_max(lst):
    max = lst[0]
    for x in lst:
        if x > max:
            max = x
    return max
